from metropolis import *
import time

if __name__ == "__main__":
	N = 20
	L = 1000
	eq_time = 1000
	J = 1
	M = 100

	rng = np.random.default_rng()
	temps = np.linspace(1, 10, 100)

	for H in [0, 0.1, 1, 10]:
		mag = []

		for kbt in temps:
			beta = 1 / kbt
			m = 0

			for i in range(M):
				random.seed(time.time())
				spins = [1 for _ in range(N)]
				# spins = [2 * rng.integers(0, 2) - 1 for _ in range(N)]
				spins_res, E = metropolis(N, L, beta, J, spins, eq_time, H)

				for conf in spins_res:
					m += magnetisation(conf) / len(spins_res)

			m /= M * N
			mag.append(m)

		plt.figure(dpi=200)
		plt.plot(temps, mag, label="Simulation")
		plt.plot(temps, np.exp(J/temps) * np.sinh(H/temps) / np.sqrt(np.exp(2*J/temps) * np.sinh(H/temps)**2 + np.exp(-2*J/temps)), alpha=0.8, label="Theory")
		plt.title(rf"$H$ = {H}")
		plt.xlabel(r"$k_B T$")
		plt.ylabel(r"$\langle m \rangle_t$")
		plt.grid(linestyle="--")
		plt.legend()
		plt.savefig(f"plots/1e_{H}.png")
		plt.show()
