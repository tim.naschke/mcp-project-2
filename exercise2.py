from metropolis2d import *
import matplotlib.pyplot as plt
from progress import progress
import time


def mag_theory(beta, J):
	z = np.exp(-2 * beta * J)

	if beta < 1 / (2.269185 * J):
		return 0
	else:
		return np.power(1 + z**2, 1/4) * np.power(1 - 6 * z**2 + z**4, 1/8) / np.sqrt(1 - z**2)


if __name__ == "__main__":
	n = 30
	L = 1000
	eq_time = 200000
	J = 1
	M = 100

	rng = np.random.default_rng()
	temps = np.linspace(1, 4, 50)
	magnetisations = []

	progress(0, 9)

	for kbt in temps:
		beta = 1 / kbt
		mag_absolute = 0

		for i in range(M):
			random.seed(time.time())
			# spins = np.array([[2 * rng.integers(0, 2) - 1 for _ in range(n)] for _ in range(n)])
			spins = np.ones((n, n))
			spins_res = metropolis2d(n, L, beta, J, spins, eq_time)

			mag = 0
			for conf in spins_res:
				mag += magnetisation(conf)

			mag_absolute += mag		# np.abs(mag)

		mag_absolute /= M * n**2 * L
		magnetisations.append(mag_absolute)

		progress(kbt - 1, 3)

	magnetisations = np.array(magnetisations)
	magnetisations.dump("data/exercise2_magnetisations.dump")

	temps_theory = np.linspace(1, 4, 1000)
	theory_line = [mag_theory(1/kbt, J) for kbt in temps_theory]

	plt.figure(dpi=200)
	plt.axvline(x=2.269185 * J, label=r"$T_C$", color="black", linestyle="--")
	plt.plot(temps, magnetisations, label="Simulation")
	plt.plot(temps_theory, theory_line, label="Theory")
	plt.xlabel(r"$k_B T$")
	plt.ylabel(r"$|\langle m \rangle|$")
	plt.grid(linestyle="--")
	plt.legend()
	plt.savefig("plots/2.png")
	plt.show()
