import numpy as np
import matplotlib.pyplot as plt
import random


def delta_e(spins, to_flip, J, H=0):
	N = len(spins)
	dE = 2 * J * (spins[(to_flip - 1) % N] * spins[to_flip % N] + spins[to_flip % N] * spins[(to_flip + 1) % N])
	dE += 2 * H * spins[to_flip % N]

	return dE


def trans_prob_index(dE, J):
	if dE == -4 * J:
		return 0
	elif dE == 0:
		return 1
	elif dE == 4 * J:
		return 2


def energy(spins, J, H=0):
	E = 0
	N = len(spins)

	for i in range(len(spins)):
		E += -J * spins[i % N] * spins[(i+1) % N]

	E += - H * magnetisation(spins)

	return E


def plot_spins(spin_evo, mod, title="", save_path=None):
	plt.figure(dpi=200)

	for i in range(len(spin_evo)):
		if i % mod == 0:
			for j in range(len(spin_evo[i])):
				plt.scatter(j + 1, i, color="blue" if spin_evo[i][j] > 0 else "red")

	plt.xlabel("particle")
	plt.ylabel("time")
	plt.title(title)

	if save_path is not None:
		plt.savefig(save_path)

	plt.show()


def magnetisation(spins):
	m = 0
	for spin in spins:
		m += spin

	return m


def metropolis(N, L, beta, J, spins_start, eq_time=0, H=0):
	spins = spins_start.copy()

	A = [np.exp(beta * 4 * J), 1, np.exp(-beta * 4 * J)]

	for _ in range(eq_time):
		n = random.randrange(N)
		dE = delta_e(spins, n, J, H)

		if H == 0:
			trans_probability = A[trans_prob_index(dE, J)]
		else:
			trans_probability = np.exp(-beta * dE)

		if random.random() < trans_probability:
			spins[n] *= -1

	spins_res = [spins.copy()]
	E = [energy(spins, J, H)]

	for _ in range(L):
		n = random.randrange(N)
		dE = delta_e(spins, n, J, H)

		if H == 0:
			trans_probability = A[trans_prob_index(dE, J)]
		else:
			trans_probability = np.exp(-beta * dE)

		if random.random() < trans_probability:
			spins[n] *= -1
			E.append(E[-1] + dE)
		else:
			E.append(E[-1])

		spins_res.append(spins.copy())

	return spins_res, E


def domains(spins):
	n_domains = 1

	for i in range(1, len(spins)):
		if spins[i] != spins[i - 1]:
			n_domains += 1

	return n_domains
