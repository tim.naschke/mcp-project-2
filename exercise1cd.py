from metropolis import *
import time

if __name__ == "__main__":
	N = 20
	L = 1000
	eq_time = 1000
	J = 1
	M = 100

	rng = np.random.default_rng()

	means = []
	spcf_heats = []
	temps = np.linspace(1, 10, 100)

	for kbt in temps:
		beta = 1 / kbt
		e_mean = 0
		e_std = 0

		for i in range(M):
			random.seed(time.time())
			spins = [1 for _ in range(N)]
			# spins = [2 * rng.integers(0, 2) - 1 for _ in range(N)]
			spins_res, E = metropolis(N, L, beta, J, spins, eq_time)

			e_mean += np.mean(E)
			e_std += np.var(E)

		e_mean /= M * N
		e_std /= M * N * kbt**2

		means.append(e_mean)
		spcf_heats.append(e_std)

	plt.figure(dpi=200)
	plt.plot(temps, means, label="Simulation")
	plt.plot(temps, -J * np.tanh(J / temps), alpha=0.8, label="Theory")
	plt.xlabel(r"$k_B T$")
	plt.ylabel(r"$\frac{1}{N} \langle E \rangle_t$")
	plt.grid(linestyle="--")
	plt.legend()
	plt.savefig("plots/1c.png")
	plt.show()

	plt.figure(dpi=200)
	plt.plot(temps, spcf_heats, label="Simulation")
	plt.plot(temps, (J / temps)**2 / (np.cosh(J / temps))**2, alpha=0.8, label="Theory")
	plt.xlabel(r"$k_B T$")
	plt.ylabel(r"$c_V$")
	plt.grid(linestyle="--")
	plt.legend()
	plt.savefig("plots/1d.png")
	plt.show()
