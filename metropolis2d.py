import numpy as np
import random


def delta_e(spins, to_flip, J):
	n = len(spins)
	flip_x, flip_y = to_flip
	flip_x, flip_y = flip_x % n, flip_y % n

	dE = 2 * J * spins[flip_x][flip_y] * (spins[(flip_x - 1) % n][flip_y] + spins[(flip_x + 1) % n][flip_y]
										+ spins[flip_x][(flip_y - 1) % n] + spins[flip_x][(flip_y + 1) % n])

	return dE


def magnetisation(spins):
	m = 0
	for row in spins:
		for spin in row:
			m += spin

	return m


def metropolis2d(n, L, beta, J, spins_start, eq_time=0):
	spins = spins_start.copy()
	spins_res = []

	for i in range(eq_time + L):
		flip_x, flip_y = random.randrange(n), random.randrange(n)
		dE = delta_e(spins, (flip_x, flip_y), J)

		if dE < 0 or random.random() < np.exp(-beta * dE):
			spins[flip_x][flip_y] *= -1

		if i >= eq_time:
			spins_res.append(spins.copy())

	return spins_res
