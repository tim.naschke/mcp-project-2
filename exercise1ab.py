import time
from metropolis import *

if __name__ == "__main__":
	N = 20
	L = 500
	J = 1

	M = 100
	plot = False

	rng = np.random.default_rng()

	for beta in [0.1, 1, 10]:
		e_mean = np.zeros(L + 1)
		e_std = np.zeros(L + 1)

		for i in range(M):
			# spins = [2 * rng.integers(0, 2) - 1 for _ in range(N)]
			random.seed(time.time())
			spins = [1 for _ in range(N)]
			spins_res, E = metropolis(N, L, beta, J, spins)

			E = np.array(E)
			e_mean += E
			e_std += E**2

			if plot:
				plot_spins(spins_res, 10, title=rf"$J$ = {J}, $k_B T$ = {1/beta}", save_path=f"plots/1a_spins_{1 / beta}.png")

				t = np.arange(0, L + 1, 1)
				plt.plot(t, E)
				plt.show()

		e_mean /= M
		e_std = np.sqrt((e_std / M - e_mean**2) / M)

		t = np.arange(0, L + 1, 1)

		plt.figure(dpi=200)
		plt.fill_between(t, e_mean - e_std, e_mean + e_std, alpha=0.9, label=r"mean $\pm$ standard error")
		plt.plot(t, e_mean, color="orange", alpha=1, label="mean")
		plt.title(rf"$k_B T$ = {1/beta}")
		plt.xlabel("time")
		plt.ylabel("energy")
		plt.grid(linestyle="--")
		plt.legend()
		plt.savefig(f"plots/1b_{1/beta}.png")
		plt.show()
