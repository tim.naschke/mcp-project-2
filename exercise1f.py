from metropolis import *
import time

if __name__ == "__main__":
	N = 20
	L = 1000
	eq_time = 1000
	J = 1
	M = 100

	rng = np.random.default_rng()
	temps = np.linspace(1, 10, 100)
	n_domains_arr = []

	for kbt in temps:
		beta = 1 / kbt
		n_domains = 0

		for i in range(M):
			random.seed(time.time())
			spins = [1 for _ in range(N)]
			# spins = [2 * rng.integers(0, 2) - 1 for _ in range(N)]
			spins_res, E = metropolis(N, L, beta, J, spins, eq_time)

			for conf in spins_res:
				n_domains += domains(conf) / len(spins_res)

		n_domains_arr.append(n_domains / M)

	plt.figure(dpi=200)
	plt.plot(temps, n_domains_arr)
	plt.xlabel(r"$k_B T$")
	plt.ylabel(r"$\langle n_{\mathrm{domains}} \rangle$")
	plt.grid(linestyle="--")
	plt.savefig("plots/1f.png")
	plt.show()
