import matplotlib.pyplot as plt
import numpy as np


def mag_theory(beta, J):
	z = np.exp(-2 * beta * J)

	if beta < 1 / (2.269185 * J):
		return 0
	else:
		return np.power(1 + z**2, 1/4) * np.power(1 - 6 * z**2 + z**4, 1/8) / np.sqrt(1 - z**2)


if __name__ == "__main__":
	n = 30
	L = 1000
	eq_time = 200000
	J = 1
	M = 100

	temps = np.linspace(1, 4, 50)
	magnetisations = np.load("data/exercise2_magnetisations.dump", allow_pickle=True)
	magnetisations = np.abs(magnetisations)

	temps_theory = np.linspace(1, 4, 1000)
	theory_line = [mag_theory(1/kbt, J) for kbt in temps_theory]

	plt.figure(dpi=200)
	plt.axvline(x=2.269185 * J, label=r"$T_C$", color="black", linestyle="--")
	plt.plot(temps, magnetisations, label="Simulation")
	plt.plot(temps_theory, theory_line, label="Theory")
	plt.xlabel(r"$k_B T$")
	plt.ylabel(r"$|\langle m \rangle|$")
	plt.grid(linestyle="--")
	plt.legend()
	plt.savefig("plots/2.png")
	plt.show()
